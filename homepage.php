<?php
/**
 * Template Name: Home
 *
 * This template contains static homepage content and the main slider
 *
 * @package WordPress
 * @subpackage Khalsa
 * @since GM 1.0
 */

get_header(); ?>

<!-- site container -->
<div class="site-container">

<?php get_template_part( 'template-parts/hero-home' ); ?>


	<!-- main content area -->
	<main class="site-main" role="main">

		<!-- section .content .content-home -->
		<section class="content content-home" id="content">

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				<!-- article -->
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<?php the_content(); ?>

				</article>
				<!-- /article -->

			<?php endwhile; ?>

			<?php else : ?>

				<!-- article -->
				<article>

					<h2><?php esc_html_e( 'Sorry, nothing to display.' ); ?></h2>

				</article>
				<!-- /article -->

			<?php endif; ?>

		</section>
		<!-- /section .content .content-home -->

	</main>
	<!-- /main content area -->

	<?php get_sidebar( 'home-upper' ); ?>

	<?php get_template_part( 'template-parts/col-cta' ); ?>

	<section id="sub-content">

		<?php if ( get_field( 'section_title' ) ) : ?>
			<h2 class="section-title"><?php the_field( 'section_title' ); ?></h2>
		<?php else : ?>
			<h2 class="section-title">Latest News</h2>
		<?php endif; ?>

		<?php if ( get_field( 'content_toggle') == 'true' ) : ?>

			<?php
			$args = array(
				'post_type'			=> 'post',
				'posts_per_page' 	=> 4,
			);
			$the_query = new WP_Query( $args ); ?>

			<?php if ( $the_query->have_posts() ) : ?>

				<ol class="post-list">

					<!-- the loop -->
					<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

						<li class="post-item">

							<!-- post thumbnail -->
							<?php if ( has_post_thumbnail() ) : // Check if thumbnail exists. ?>
								<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
									<?php the_post_thumbnail( 'small' ); // Declare pixel size you need inside the array. ?>
								</a>
							<?php endif; ?>
							<!-- /post thumbnail -->

							<!-- post title -->
							<h3 class="post-title">
								<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
							</h3>
							<!-- /post title -->

							<?php the_excerpt(); ?>

						</li>

					<?php endwhile; ?>
					<!-- end of the loop -->

				</ol>

				<?php wp_reset_postdata(); ?>

			<?php else : ?>
				<p><?php esc_html_e( 'Check back later for more updates!' ); ?></p>
			<?php endif; ?>

		<?php else : ?>

			<?php get_field( 'sub_content' ); ?>

		<?php endif; ?>

	</section>

	<?php get_sidebar( 'home-lower' ); ?>

	<?php get_footer(); ?>
