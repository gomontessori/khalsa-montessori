<?php
/**
 * Site footer
 *
 * This template contains all footer information and the end of the HTML doc.
 *
 * @package WordPress
 * @subpackage Khalsa
 * @since GM 1.0
 */

?>

<!-- footer -->
<footer class="footer" role="contentinfo">

	<div class="footer-top">
		<div class="footer-left">
			<?php if ( has_nav_menu( 'primary' ) ) : ?>
				<nav id="footer-navigation" class="footer-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer Menu' ); ?>">
					<?php gm_footer_nav(); ?>
				</nav>
			<?php endif; ?>
		</div>
		<aside class="sidebar footer-side">
			<?php if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'footer-widgets' ) ) ?>
			<div id="social-links-menu" class="social-links-container">
				<?php if ( has_nav_menu( 'social' ) ) : ?>
					<nav id="social-navigation" class="social-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Social Media Menu' ); ?>">
						<?php gm_nav_social(); ?>
					</nav>
				<?php endif; ?>
			</div>
		</aside>
	</div>

	<?php get_template_part( 'template-parts/accreditations' ); ?>

	<div class="footer-bottom">

		<div class="footer-bottom-left">

			<?php get_template_part( 'template-parts/contact-loop' ); ?>

		</div>

		<div class="footer-bottom-right copyright">

			<?php if ( get_field( 'terms', 'option' ) ) : ?>
				<div class="terms">
					<?php echo wp_kses_post( get_field( 'terms', 'option' ) ); ?>
				</div>
			<?php endif; ?>

			<p>
				&copy; <?php echo esc_html( date( 'Y' ) ); ?> Copyright <?php bloginfo( 'name' ); ?>. All Rights Reserved<br>
				<?php esc_html_e( 'Powered by ' ); ?><a href="http://gomontessori.com" target="_blank" rel="nofollow">Go Montessori</a>
			</p>
		</div>

	</div>

</footer>
<!-- /footer -->

</div>
<!-- /content container -->

</div>
<!-- /site container -->


<?php wp_footer(); ?>

</body>
</html>
