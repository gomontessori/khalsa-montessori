module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        copy: {
            main: {
                files: [
                    {src: ['src/bower_components/normalize.css/normalize.css'], dest: 'assets/css/normalize.css'},
                    //{src: ['src/bower_components/svgxuse/svgxuse.min.js'], dest: 'src/js/lib/svgxuse.min.js'},
                    {src: ['src/bower_components/webfontloader/webfontloader.js'], dest: 'src/js/lib/webfontloader.js'},
                ]
            }
        },

        sass: {                              // Task
            dist: {                            // Target
                options: {                       // Target options
                    style: 'expanded',
                    require: 'susy',
                    compass: true,
                },
                files: {                         // Dictionary of files
                    'assets/css/style.css': 'src/sass/style.scss',
                    'editor-style.css' : 'src/sass/editor-style.scss',
                }
            }
        },

        modernizr: {
            dist: {
                "crawl": false,
                "dest": "src/js/lib/modernizr-output.js",
                "tests": [
                    "svg",
                    "video",
                    "backgroundsize",
                    "bgsizecover",
                    "borderradius",
                    "fontface",
                    "lastchild",
                    "nthchild",
                    "cssremunit",
                    "rgba"
                ],
                "options": [
                    "html5shiv",
                    "setClasses"
                ],
                "uglify": true
            }
        },

        concat: {
            options: {
                // define a string to put between each file in the concatenated output
                separator: ';'
            },
            dist: {
                // the files to concatenate
                src: ['src/js/lib/*.js', 'src/js/scripts.js'],
                // the location of the resulting JS file
                dest: 'assets/js/scripts.all.js'
            }
        },

        uglify: {
            my_target: {
                files:
                    {
                        'assets/js/scripts.all.min.js': 'assets/js/scripts.all.js',
                        'assets/js/fonts.min.js': 'src/js/fonts.js'
                    }
            }
        },

        svg2png: {
            all: {
                // specify files in array format with multiple src-dest mapping
                files: [
                    // rasterize all SVG files in "img" and its subdirectories to "img/png"
                    { cwd: 'src/img/', src: ['*.svg'], dest: 'assets/img/' }
                ]
            }
        },

        svgmin: {
            options: {
                plugins: [
                    { removeViewBox: false },               // don't remove the viewbox atribute from the SVG
                    { removeUselessStrokeAndFill: false },  // don't remove Useless Strokes and Fills
                    { removeEmptyAttrs: false },            // don't remove Empty Attributes from the SVG
                    { convertTransform: false }
                ]
            },
            dist: {
                expand: true,
                cwd: 'src/img',
                src: ['*.svg'],
                dest: 'assets/img',
            }
        },


        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'src/img',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'assets/img'
                }]
            }
        },


        postcss: {
            options: {
                map: false,
                processors: [
                    require('autoprefixer')({browsers: 'last 1 version'}),
                    require('lost')
                ]
            },
            dist: {
                src: 'assets/css/style.css'
            }
        },

        watch: {
            scripts: {
                files: ['src/js/**/*.js'],
                tasks: ['concat', 'uglify'],
                options: {
                    spawn: false,
                }
            },

            css: {
                files: ['src/sass/**/*.scss'],
                tasks: ['sass', 'postcss'],
                options: {
                    spawn: false,
                }
            }
        },

        realFavicon: {
            favicons: {
                src: 'favicon.png',
                dest: 'assets/img/icons',
                options: {
                    iconsPath: '<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/assets/img/icons/',
                    html: [ 'assets/img/icons/favicons.html' ],
                    design: {
                        ios: {
                            pictureAspect: 'backgroundAndMargin',
                            backgroundColor: '#ffffff',
                            margin: '14%',
                            assets: {
                                ios6AndPriorIcons: false,
                                ios7AndLaterIcons: false,
                                precomposedIcons: false,
                                declareOnlyDefaultIcon: true
                            }
                        },
                        desktopBrowser: {},
                        windows: {
                            pictureAspect: 'noChange',
                            backgroundColor: '#00aba9',
                            onConflict: 'override',
                            assets: {
                                windows80Ie10Tile: false,
                                windows10Ie11EdgeTiles: {
                                    small: false,
                                    medium: true,
                                    big: false,
                                    rectangle: false
                                }
                            }
                        },
                        androidChrome: {
                            pictureAspect: 'backgroundAndMargin',
                            margin: '17%',
                            backgroundColor: '#ffffff',
                            themeColor: '#ffffff',
                            manifest: {
                                name: 'Khalsa Montessori',
                                display: 'standalone',
                                orientation: 'notSet',
                                onConflict: 'override',
                                declared: true
                            },
                            assets: {
                                legacyIcon: false,
                                lowResolutionIcons: false
                            }
                        }
                    },
                    settings: {
                        compression: 5,
                        scalingAlgorithm: 'Lanczos',
                        errorOnImageTooSmall: false
                    }
                }
            }
        }


    });

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks("grunt-modernizr");
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-svg2png');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-svgmin');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-preen');
    grunt.loadNpmTasks('grunt-real-favicon');


    // Default task(s).
    grunt.registerTask('default', ['copy', 'sass', 'modernizr','concat', 'uglify', 'svg2png', 'svgmin', 'imagemin', 'postcss', 'watch', 'preen', 'realFavicon']);

};
