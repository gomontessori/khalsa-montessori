<?php
/**
 * Home Sidebar (Lower)
 *
 * This template is lower sidebar on the homepage template.
 *
 * @package WordPress
 * @subpackage Khalsa
 * @since GM 1.0
 */

?>
<!-- sidebar -->
<aside class="sidebar home-lower" role="complementary">
	
	<?php if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'sidebar-home-lower' ) ) ?>

</aside>
<!-- /sidebar -->
