<?php
/**
 * Blog sidebar
 *
 * This sidebar appears on blog archives and single posts.
 *
 * @package WordPress
 * @subpackage Khalsa
 * @since GM 1.0
 */

?>
<!-- sidebar -->
<aside class="sidebar sidebar-blog" role="complementary">
	
	<?php if ( !function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'blog-widgets' ) ) ?>

</aside>
<!-- /sidebar -->
