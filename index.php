<?php
/**
 * Blog Index
 *
 * This template contains main blog index structure.
 *
 * @package WordPress
 * @subpackage Khalsa
 * @since GM 1.0
 */

?>
<?php get_header(); ?>

<!-- site container -->
<div class="site-container">

<?php get_template_part( 'template-parts/hero' ); ?>

	<!-- main content area -->
	<main class="site-main" role="main">

		<!-- section -->
		<section class="page-content" id="content">

			<h1>
				<?php $blogid = get_option( 'page_for_posts' );
				echo get_the_title( $blogid ); ?>
			</h1>

			<?php get_template_part( 'template-parts/loop' ); ?>

			<?php get_template_part( 'template-parts/pagination' ); ?>

		</section>
		<!-- /section -->

	</main>
	<!-- /main content area -->

	<?php get_sidebar( 'blog' ); ?>

	<?php get_footer(); ?>
