<?php
/**
 * Custom Sidebar menu
 *
 * This template contains the widget with the customized sidebar menu.
 *
 * @package WordPress
 * @subpackage Khalsa
 * @since GM 1.0
 */

?>

<!-- sidebar page menu - sibling pages only show on child page -->

<?php
// Before dynamic page list sidebar links.
$beforelinks = get_field( 'sidebar_links_before' );

// After dynamic page list sidebar links.
$afterlinks = get_field( 'sidebar_links_after' );

if ( $post->post_parent ) {
	$children = wp_list_pages( array(
		'title_li' => '',
		'child_of' => $post->post_parent,
		'echo'     => 0,
	) );
	$title = get_the_title( $post->post_parent );
} else {
	$children = wp_list_pages( array(
		'title_li' => '',
		'child_of' => $post->ID,
		'echo'     => 0,
	) );
	$title = get_the_title( $post->ID );
}
?>


	<div class="widget">

		<?php if ( $beforelinks || $afterlinks || $children ) : ?>
		<?php if ( get_field( 'sidebar_menu_title' ) ) : ?>
			<h2 class="widgettitle"><?php the_field( 'sidebar_menu_title' ); ?></h2>
		<?php else : ?>
			<h2 class="widgettitle">Quick Links</h2>
		<?php endif; ?>

		<!-- section title -->
		<?php /* if ( $title ) {
			echo '<h3 class="menutitle" >';
			echo esc_html( $title );
			echo '</h3 >';
		  } */ ?>

		<nav class="side-nav">

			<ul class="side-menu">

				<?php if ( $beforelinks ) {
					foreach ( $beforelinks as $beforelink ) {
						echo '<li><a href="' . esc_url( $beforelink['sidebar_before_link_url']['url'] ) . '">';
						echo esc_html( $beforelink['sidebar_before_link_url']['title'] ) . '</a></li>';
					}
				} ?>
				<?php if ( $children ) {  // list child pages as defined above.
					echo $children; } ?>
				<?php if ( $afterlinks ) {
					foreach ( $afterlinks as $afterlink ) {
						echo '<li><a href="' . esc_url( $afterlink['sidebar_after_link_url']['url'] ) . '">';
						echo esc_html( $afterlink['sidebar_after_link_url']['title'] ) . '</a></li>';
					}
				} ?>

			</ul>
		</nav>

	</div>
<?php endif; ?>
