<?php
/**
 * Homepage Hero template part.
 *
 * This template part contains the hero banner image featured on the homepage only.
 *
 * @package WordPress
 * @subpackage Khalsa Template 1
 * @since GM Template 1 1.0
 */

?>

<!-- post thumbnail or slideshow -->
<?php if ( get_field( 'slideshow_toggle' ) == 'dynamic' || get_field( 'slideshow_toggle' ) == 'static' ) : ?>
	<!-- hero slideshow-->
		<!-- custom slider in functions -->
		<?php gm_khalsa_slider( ); ?>
	<!--/hero slideshow-->
<?php elseif ( get_field( 'slideshow_toggle' ) == 'video' ) : ?>
	<div class="hero hero-home hero-video">
		<?php if ( get_field( 'before_video_text' ) ) : ?>
			<div class="video-caption">
				<?php echo wp_kses_post( get_field( 'before_video_text' ) ); ?>
			</div>
		<?php endif; ?>
		<?php if ( get_field( 'video_link' ) ) : ?>
			<div class="embed-outer">
				<div class="embed-wrapper">
					<div class="video-player" data-id="<?php the_field( 'video_link' ); ?>">
						<?php if ( get_field( 'video_source') == 'youtube' ) : ?>
							<img src="https://i.ytimg.com/vi/<?php the_field( 'video_link' ); ?>/hqdefault.jpg" />
						<?php elseif ( get_field( 'video_source') == 'vimeo' ) : ?>
							<img src="http://i.vimeocdn.com/video/<?php the_field( 'video_link' ); ?>_960.jpg" />
						<?php endif; ?>
						<div class="play"></div>
					</div>
				</div>
			</div>
		<?php endif; ?>

	</div>
<?php elseif ( has_post_thumbnail() ) : // Check if thumbnail exists.
	global $post;
	$src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'hero' );
	$style = 'style="background-image: url(' . esc_url( $src[0] ) . ') !important;"';
else :
	$style = '';
	$class = 'hero-default';
?>
<div class="hero hero-home <?php echo esc_attr( $class ); ?>" <?php echo esc_attr( $style ); ?>></div>
<!-- content container -->
<div class="content-container">
	<div class="main-cta-text">
		<?php if ( get_field( 'main_cta_text' ) ) : ?>
			<?php echo wp_kses_post( get_field( 'main_cta_text' ) ); ?>
		<?php endif; ?>
		<?php $url = get_field( 'link_cta' );
		if ( $url ) : ?>
			<a class="btn-main-cta" href="<?php echo esc_url( $url['url'] ); ?>"><?php echo esc_html( $url['title'] ); ?></a>
		<?php endif; ?>
	</div>
<?php endif; ?>



	<!-- /banner/slider -->
