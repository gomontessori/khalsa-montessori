<?php
/**
 * Homepage multi-column CTA
 *
 * This template contains a CTA banner with multiple columns (locations)
 *
 * @package WordPress
 * @subpackage Khalsa
 * @since GM 1.0
 */

?>

<!-- section cta columns -->
<section id="locations" class="clear">

	<h2 class="section-title"><?php echo esc_html( get_field( 'section_header' ) ); ?></h2>

	<?php if ( have_rows( 'columns' ) ) : ?>

		<?php while ( have_rows( 'columns' ) ) : the_row(); ?>

			<div class="col">

				<article class="column-content">

					<?php echo wp_kses_post( get_sub_field( 'column_content' ) ); ?>

				</article>

			</div>

		<?php endwhile; ?>

	<?php endif; ?>


</section>
<!-- section cta columns -->
