<?php
/**
 * Default Hero template part.
 *
 * This template part contains the hero banner image featured at the top of non-single posts such as archive or index pages.
 *
 * @package WordPress
 * @subpackage Khalsa Template 2
 * @since GM Template 1 1.0
 */

?>

<div class="sub-hero hero-default"></div>
<!-- content container -->
<div class="content-container">
	<div class="sub-quote">
		<blockquote>The child is both a hope and promise for man kind.</blockquote><br>
		<cite>Maria Montessori</cite>
	</div>
