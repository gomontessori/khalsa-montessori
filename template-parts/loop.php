<?php
/**
 * Loop
 *
 * The main loop for all blog posts.
 *
 * @package WordPress
 * @subpackage Khalsa Template 1
 * @since GM Template 1 1.0
 */

?>
<ol class="post-list">

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<li class="post-item">

		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<!-- post thumbnail -->
			<?php if ( has_post_thumbnail() ) : // Check if thumbnail exists. ?>
				<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
					<?php the_post_thumbnail( 'small' ); // Declare pixel size you need inside the array. ?>
				</a>
			<?php endif; ?>
			<!-- /post thumbnail -->

			<!-- post title -->
			<h2 class="post-title">
				<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
			</h2>
			<!-- /post title -->
            
            <span class="post-meta"><?php the_time( 'F j, Y' ); ?></span>

			<?php the_excerpt(); ?>

		</article>
		<!-- /article -->

	<?php endwhile; ?>

	<?php else : ?>

		<!-- article -->
		<article class="post">
			<p><?php esc_html_e( 'Check back for more news and updates in the near future!' ); ?></p>
		</article>
		<!-- /article -->

	<?php endif; ?>

	</li>

</ol>
