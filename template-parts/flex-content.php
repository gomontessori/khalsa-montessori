<?php
/**
 * Flex content
 *
 * This template contains the flex content structure defined in ACF
 *
 * @package WordPress
 * @subpackage Khalsa
 * @since GM 1.0
 */

?>


<?php if ( have_rows( 'flex_content' ) ) : ?>

	<?php while ( have_rows( 'flex_content' ) ) : the_row(); ?>

		<?php if ( get_row_layout() == 'text' ) :

			$ctext = get_sub_field( 'text_content' );

			if ( $ctext ) : ?>

				<?php echo $ctext; ?>

			<?php endif; ?>

		<?php elseif ( get_row_layout() == 'accordion_list' ) : ?>

			<?php if ( get_sub_field( 'list_section_title' ) ) {
				echo '<h2>' . esc_html( get_sub_field( 'list_section_title' ) ) . '</h2>';
			}; ?>
			<?php if ( have_rows( 'list' ) ) : ?>
				<ul class="accordion-list">
					<?php while ( have_rows( 'list' ) ) : the_row();
						$listtitle = get_sub_field( 'list_item_title' );
						$listcontent = get_sub_field( 'list_item_content' );
						?>
						<li class="accordion-item">
							<h3 class="accordion-title"><a href="#"><?php echo esc_html( $listtitle ); ?></a></h3>
							<div class="accordion-content">
								<?php echo wp_kses_post( $listcontent ); ?>
							</div>
						</li>
					<?php endwhile; ?>
				</ul>
			<?php endif; ?>

		<?php elseif ( get_row_layout() == 'features_table' ) : ?>


			<h2 class="features-title"><?php the_sub_field( 'table_section_title' ); ?></h2>

			<?php $table = get_sub_field( 'table' );

			if ( $table ) {

				echo '<table border="0">';

				if ( $table['header'] ) {

					echo '<thead>';

					echo '<tr>';

					foreach ( $table['header'] as $th ) {

						echo '<th>';
						echo $th['c'];
						echo '</th>';
					}

					echo '</tr>';

					echo '</thead>';
				}

				echo '<tbody>';

				foreach ( $table['body'] as $tr ) {

					echo '<tr>';

					foreach ( $tr as $td ) {

						echo '<td>';
						echo $td['c'];
						echo '</td>';
					}

					echo '</tr>';
				}

				echo '</tbody>';

				echo '</table>';

			}; ?>

		<?php endif; ?>

	<?php endwhile;
endif; ?>
