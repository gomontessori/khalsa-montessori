<?php
/**
 * Contact Info Loop
 *
 * This template contains the loop for contact info for businesses with multiple locations
 *
 * @package WordPress
 * @subpackage Khalsa
 * @since GM 1.0
 */

?>

<div class="contact-info">
<?php if ( have_rows( 'facilities', 'option' ) ) : ?>

	<?php while ( have_rows( 'facilities', 'option' ) ) : the_row(); ?>

		<div class="facility">
			<?php if ( get_sub_field( 'facility_name' ) ) : ?>
				<h3 class="contact-title">
					<?php the_sub_field( 'facility_name' ); ?>
				</h3>
			<?php endif; ?>
			<?php if ( get_sub_field( 'address' ) ) : ?>
				<span class="contact-item">
					<a target="_blank" href="https://www.google.com/maps/place/<?php echo esc_html( preg_replace( '/\s+/', '+', get_sub_field( 'address' ) ) ); ?>"><?php the_sub_field( 'address' ); ?></a>
				</span>
			<?php endif; ?>
			<?php if ( get_sub_field( 'phone' ) ) : ?>
				<span class="contact-item">
					<?php esc_html_e( 'Phone: ' ); ?>
					<a href="tel:<?php echo esc_html( preg_replace( '/\D+/', '', get_sub_field( 'phone' ) ) ); ?>"><?php the_sub_field( 'phone' ); ?></a>
				</span>
			<?php endif; ?>
			<?php if ( get_sub_field( 'fax' ) ) : ?>
				<span class="contact-item">
					<?php esc_html_e( 'Fax: ' ); ?><?php the_sub_field( 'fax' ); ?>
				</span>
			<?php endif; ?>
			<?php if ( get_sub_field( 'email' ) ) : ?>
				<span class="contact-item">
					<a href="mailto:<?php the_sub_field( 'email' ) ?>">Email Us</a>
				</span>
			<?php endif; ?>
			<?php if ( get_sub_field( 'description' ) ) : ?>
				<span class="contact-item">
					<?php the_sub_field( 'description' ); ?>
				</span>
			<?php endif; ?>
			<?php if ( get_sub_field( 'contact_link' ) ) :
				$link = get_sub_field( 'contact_link' ) ?>
				<a class="btn-green" href="<?php echo esc_url( $link['url'] ); ?>"><?php echo esc_html( $link['title'] ); ?></a>
			<?php endif; ?>
		</div>

	<?php endwhile; ?>

<?php endif; ?>
</div>

