<?php
/**
 * Hero template part.
 *
 * This template part contains the hero banner image featured at the top of every page/post.
 *
 * @package WordPress
 * @subpackage Khalsa Template 1
 * @since GM Template 1 1.0
 */

?>

<?php global $post;
if ( is_home() ) {
	$id = get_option( 'page_for_posts' );
	$src = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'sub-hero' );
} else {
	$id = $post->ID;
	$src = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'sub-hero' );
}
if ( has_post_thumbnail( $id ) ) : // Check if thumbnail exists.
	$style = 'style="background-image: url(' . esc_url( $src[0] ) . ') !important;"';
	$class = '';
else :
$style = '';
$class = 'hero-default';
?>
<div class="sub-hero <?php echo esc_attr( $class ); ?>" <?php echo esc_attr( $style ); ?>></div>
	<!-- content container -->
	<div class="content-container">
		<div class="sub-quote">
			<?php if ( get_field( 'quotation' ) ) {
				echo '<blockquote>' . wp_kses_post( get_field( 'quotation' ) ) . '</blockquote>'; } else {
				echo '<blockquote>Mindful Activity within a compassionate community for personal growth and academic excellence.</blockquote>';
			}?>
			<?php if ( get_field( 'citation' ) ) {
				echo '<cite>' . esc_html( get_field( 'citation' ) ) . '</cite>'; } ?>
		</div>
<?php endif; ?>
