<?php
/**
 * Accreditations/Affiliations
 *
 * This template contains the accreditations/corporate/affiliate logos repeater defined in ACF > Options
 *
 * @package WordPress
 * @subpackage Khalsa
 * @since GM 1.0
 */

?>

<?php if ( have_rows( 'corporate_logos', 'option' ) ) : ?>
	<div class="accreditations">
		<ul class="accred-list">
			<?php while ( have_rows( 'corporate_logos', 'option' ) ) : the_row();
				$logoimage = get_sub_field( 'logo_image' );
				$logolink = get_sub_field( 'link' );
				?>
				<li class="accred-item">
					<a href="<?php echo esc_url( $logolink ); ?>" target="_blank">
						<img src="<?php echo esc_url( $logoimage['url'] ); ?>" alt="<?php echo esc_attr( $logoimage['url'] ); ?>" class="img-accred">
					</a>
				</li>
			<?php endwhile; ?>
		</ul>
	</div>
<?php endif; ?>
