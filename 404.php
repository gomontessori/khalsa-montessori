<?php
/**
 * 404 Template
 *
 * This template is for 404 (not found) error pages
 *
 * @package WordPress
 * @subpackage Khalsa
 * @since GM 1.0
 */

?>
<?php get_header(); ?>

<!-- site container -->
<div class="site-container">

<?php get_template_part( 'template-parts/hero-default' ); ?>

	<!-- main content area -->
	<main class="site-main" role="main">

		<!-- section -->
		<section class="page-content" id="content">

			<!-- article -->
			<article id="post-404">

				<header class="entry-title">
					<h1><?php esc_html_e( 'Page not found' ); ?></h1>
				</header>

				<h2>
					<a href="<?php echo esc_url( home_url() ); ?>"><?php esc_html_e( 'Return home?' ); ?></a>
				</h2>
				<p>Or try something else?</p>
				<?php get_search_form(); ?>

			</article>
			<!-- /article -->

		</section>
		<!-- /section -->

	</main>
	<!-- /main content area -->

	<?php get_footer(); ?>
