<?php
/**
 * Single blog post
 *
 * This template contains the content for a single blog post
 *
 * @package WordPress
 * @subpackage Khalsa
 * @since GM 1.0
 */

?>
<?php get_header(); ?>

<!-- site container -->
<div class="site-container">

<?php get_template_part( 'template-parts/hero' ); ?>

	<!-- main content area -->
	<main class="site-main" role="main">

		<!-- section -->
		<section class="page-content" id="content">

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				<!-- article -->
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<h1><?php the_title(); ?></h1>

					<?php the_content(); ?>

					<?php comments_template(); ?>

				</article>
				<!-- /article -->

			<?php endwhile; ?>

			<?php else : ?>

				<!-- article -->
				<article>

					<h2><?php esc_html_e( 'Sorry, nothing to display.' ); ?></h2>

				</article>
				<!-- /article -->

			<?php endif; ?>
		</section>
		<!-- /section -->

	</main>
	<!-- /main content area -->

	<?php get_sidebar( 'blog' ); ?>

<?php get_footer(); ?>
