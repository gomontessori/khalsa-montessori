<?php
/**
 * Tag archive
 *
 * This template is for posts archived by tag.
 *
 * @package WordPress
 * @subpackage Khalsa
 * @since GM 1.0
 */

?>
<?php get_header(); ?>

<!-- site container -->
<div class="site-container">

<?php get_template_part( 'template-parts/hero-default' ); ?>

	<!-- main content area -->
	<main class="site-main" role="main">

		<!-- section -->
		<section class="page-content" id="content">

			<h1><?php esc_html_e( 'Tag Archive: ' );
				echo single_tag_title( '', false ); ?></h1>

			<?php get_template_part( 'template-parts/loop' ); ?>

			<?php get_template_part( 'template-parts/pagination' ); ?>

		</section>
		<!-- /section -->

	</main>
	<!-- /main content area -->

	<?php get_sidebar( 'blog' ); ?>

	<?php get_footer(); ?>
