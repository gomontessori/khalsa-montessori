<?php
/**
 * Site header
 *
 * This template contains all site head information up through the beginning of the site content.
 *
 * @package WordPress
 * @subpackage Khalsa
 * @since GM 1.0
 */

?>
<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head itemscope itemtype="http://schema.org/WebSite">
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<title itemprop='name'><?php wp_title( ' | ' ); ?></title>
	<link rel="canonical" href="<?php echo esc_url( home_url() ); ?>" itemprop="url">

	<link href="//www.google-analytics.com" rel="dns-prefetch">

	<!--Favicons-->
	<?php include_once( 'assets/img/icons/favicons.html' ); ?>
	<!-- /Favicons -->

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>

<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content' ); ?></a>

<!-- header -->
<header id="masthead" class="site-header clear" role="banner">

	<div class="content-container">

		<!-- announcement -->
		<?php if ( get_field( 'announcement', 'option' ) ) : ?>
			<div class="announcement">
					<p><?php the_field( 'announcement', 'option' ); ?>
						<?php if ( get_field( 'announcement_url', 'option' ) ) : ?>
							... <a href="<?php the_field( 'announcement_url', 'option' ); ?>">Read More</a>
						<?php endif; ?>
					</p>
			</div>
		<?php endif; ?>
		<!-- /announcement -->

		<div class="top-bar">
				<nav id="top-navigation" class="top-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Top Menu' ); ?>">
					<?php gm_top_nav(); ?>
				</nav>
		</div>
		<div id="header-left">

			<!-- logo -->
			<?php if ( get_field( 'logo_toggle', 'option' ) ) : ?>
				<div class="logo">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
						<img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/assets/img/logo.png" alt="Logo" class="logo-img">
					</a>
				</div>
			<?php endif; ?>
			<!-- /logo -->

			<div class="title-container">
				<?php if ( get_field( 'title_toggle', 'option' ) ) {
					$display = '';
				} else {
					$display = 'screen-reader-text';
				}

				if ( is_front_page() ) {
					echo '<h1 class="site-title ' . esc_attr( $display ) . '"><a href="' . esc_url( home_url( '/' ) ) . '" rel="home">' . esc_html( get_bloginfo( 'name' ) ) . '</a></h1>';
				} else {
					echo '<p class="site-title ' . esc_attr( $display ) . '"><a href="' . esc_url( home_url( '/' ) ) . '" rel="home">' . esc_html( get_bloginfo( 'name' ) ) . '</a></p>';
				}

				if ( get_field( 'tagline_toggle', 'option' ) ) {
					if ( get_field( 'header_tagline', 'option' ) ) {
						echo '<p class="header-tagline">' . esc_html( get_field( 'header_tagline', 'option' ) ) . '</p>';
					} else {
						echo '<p class="header-tagline">' . esc_html( get_bloginfo( 'description' ) ) . '</p>';
					};
				}
				?>
			</div>
		</div>

		<!-- main navigation -->
		<div class="site-menu-container">

			<button id="menu-toggle" class="menu-toggle"><?php esc_html_e( 'Menu' ); ?></button>

			<div id="site-header-menu" class="site-header-menu">
				<?php if ( has_nav_menu( 'primary' ) ) : ?>
					<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Primary Menu' ); ?>">
						<?php gm_nav_main(); ?>
					</nav>
				<?php endif; ?>
			</div>

		</div>
		<!-- /main navigation -->

	</div>

</header>
<!-- /header -->
