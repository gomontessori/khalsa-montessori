<?php
/**
 * Author: Go Montessori
 *
 * URL: http://gomontessori.com
 * Credits: html5-blank by Todd Motto
 */

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

if ( !isset( $content_width ) )
{
	$content_width = 1280;
}

if ( function_exists( 'add_theme_support' ) )
{

	// Add Thumbnail Theme Support
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'hero', 1490, 700, array( 'center', 'center' ) ); // Homepage hero image.
	add_image_size( 'sub-hero', 1490, 400, array( 'center', 'center' ) ); // Subpage hero image.

	// Add Support for Custom Backgrounds - Uncomment below if you're going to use
	/*add_theme_support('custom-background', array(
	'default-color' => 'FFF',
	'default-image' => get_template_directory_uri() . '/img/bg.jpg'
	));*/

	// Add Support for Custom Header - Uncomment below if you're going to use
	/*add_theme_support('custom-header', array(
	'default-image'          => get_template_directory_uri() . '/img/headers/default.jpg',
	'header-text'            => false,
	'default-text-color'     => '000',
	'width'                  => 1000,
	'height'                 => 198,
	'random-default'         => false,
	'wp-head-callback'       => $wphead_cb,
	'admin-head-callback'    => $adminhead_cb,
	'admin-preview-callback' => $adminpreview_cb
	));*/

	// Enables post and comment RSS feed links to head
	add_theme_support( 'automatic-feed-links' );

	// Add Support for HTML5
	add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
}

/*------------------------------------*\
	Functions
\*------------------------------------*/

/** Custom main navigation */
function gm_nav_main() {
	wp_nav_menu(
		array(
			'theme_location'  => 'primary',
			'menu_class'      => 'primary-menu',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
			'depth'           => 0,
			'container'       => false,
		)
	);
}

/** Custom social media navigation */
function gm_nav_social() {
	wp_nav_menu(
		array(
			'theme_location' => 'social',
			'menu_class'     => 'social-links-menu',
			'depth'          => 1,
			'link_before'    => '<span class="screen-reader-text">',
			'link_after'     => '</span>',
			'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
			'container'       => false,
		)
	);
}

/** Custom top bar navigation */
function gm_top_nav() {
	wp_nav_menu(
		array(
			'theme_location' => 'top-bar',
			'menu_class'     => 'top-menu',
			'echo'            => true,
			'depth'          => 0,
			'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
			'container'       => false,
		)
	);
}

/** Custom footer navigation */
function gm_footer_nav() {
	wp_nav_menu(
		array(
			'theme_location' => 'footer',
			'menu_class'     => 'footer-menu',
			'echo'            => true,
			'depth'          => 0,
			'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
			'container'       => false,
		)
	);
}

/** Register Custom Navigation */
function gm_register_menu()
{
	register_nav_menus(array( // Using array to specify more menus if needed.
		'primary' => __( 'Header Menu' ), // Main Navigation.
		'social' => __( 'Social Menu' ),// Social links.
		'top-bar' => __( 'Top Menu' ), // Top Bar navigation.
		'footer' => __( 'Footer Menu' ), // Footer navigation.
	));
}
add_action( 'init', 'gm_register_menu' );


/** Register theme scripts and styles */
function gm_add_theme_scripts() {

	wp_enqueue_style( 'normalize', get_template_directory_uri() . '/assets/css/normalize.css' );

	wp_enqueue_style( 'style', get_template_directory_uri() . '/assets/css/style.css', 'normalize' );

	wp_enqueue_script( 'gm-font-awesome', 'https://use.fontawesome.com/82cd33f513.js' );

	wp_enqueue_script( 'script', get_template_directory_uri() . '/assets/js/scripts.all.min.js', array( 'jquery' ), 1.1, true );

	wp_enqueue_script( 'fonts', get_template_directory_uri() . '/assets/js/fonts.min.js', array( 'jquery', 'script' ), 1.1, true );

	wp_localize_script( 'script', 'screenReaderText', array(
		'expand'   => __( 'expand child menu', 'gm-blank' ),
	) );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'gm_add_theme_scripts' );

/** Register widget areas */
if ( function_exists( 'register_sidebar' ) ) {

	/*register_sidebar( array(
		'name' => __( 'Header Widgets' ),
		'description' => __( 'Right side header top bar widgets' ),
		'id' => 'header-widgets',
		'before_widget' => '<div id="%1$s" class="%2$s widget">',
		'after_widget' => '</div>',
	));*/
	register_sidebar( array(
		'name' => __( 'Sidebar Widgets' ),
		'description' => __( 'Page sidebar widgets' ),
		'id' => 'sidebar-widgets',
		'before_widget' => '<div id="%1$s" class="%2$s widget">',
		'after_widget' => '</div>',
	));
	register_sidebar( array(
		'name' => __( 'Home Upper Widgets' ),
		'description' => __( 'Home upper sidebar widgets' ),
		'id' => 'sidebar-home-upper',
		'before_widget' => '<div id="%1$s" class="%2$s widget">',
		'after_widget' => '</div>',
	));
	register_sidebar( array(
		'name' => __( 'Home Lower Widgets' ),
		'description' => __( 'Home lower sidebar widgets' ),
		'id' => 'sidebar-home-lower',
		'before_widget' => '<div id="%1$s" class="%2$s widget">',
		'after_widget' => '</div>',
	));
	/* register_sidebar( array(
		'name' => __( 'Home Widgets' ),
		'description' => __( 'Home sidebar widgets' ),
		'id' => 'home-widgets',
		'before_widget' => '<div id="%1$s" class="%2$s widget">',
		'after_widget' => '</div>',
	)); */
	register_sidebar( array(
		'name' => __( 'Blog Widgets' ),
		'description' => __( 'Blog sidebar widgets (appears on blog archives and single posts)' ),
		'id' => 'blog-widgets',
		'before_widget' => '<div id="%1$s" class="%2$s widget">',
		'after_widget' => '</div>',
	));
	register_sidebar( array(
		'name' => __( 'Footer Widgets' ),
		'description' => __( 'Footer widgets (appear in right side of footer)' ),
		'id' => 'footer-widgets',
		'before_widget' => '<div id="%1$s" class="%2$s widget">',
		'after_widget' => '</div>',
	));
	// ACF generated widget areas.
	/*include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	if ( is_plugin_active( 'advanced-custom-fields-pro/acf.php' ) ) { // Check to see if ACF is installed.
		$post_id = get_option( 'page_on_front' );
		if ( get_field( 'columns', $post_id ) ) {
			while ( has_sub_field( 'columns', $post_id ) ) { // Loop through sidebar fields to generate custom sidebars.
				if ( get_sub_field( 'widget_toggle', $post_id ) ) {
					$s_name = get_sub_field( 'column_header', $post_id );
					$s_id   = preg_replace( '/[^a-zA-Z0-9\s]/', '', $s_name );
					$s_id   = preg_replace( '/ +/', ' ', $s_id );
					$s_id   = str_replace( ' ', '-', $s_id ); // Replaces spaces in Sidebar Name to dash.
					$s_id   = strtolower( $s_id ); // Transforms edited Sidebar Name to lowercase.
					register_sidebar( array(
						'name'          => 'Homepage: ' . $s_name,
						'id'            => $s_id,
						'before_widget' => '<div id="%1$s" class="widget %2$s">',
						'after_widget'  => '</div>',
					) );
				};
			};
		};
	};*/
}

/** Custom pagination for posts, CPTs */
function gm_wp_pagination() {
	global $wp_query;
	$big = 999999999;
	$translated = __( 'Page' ); // Supply translatable string.
	echo paginate_links( array(
		'base'      => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
		'format'    => '?paged=%#%',
		'current'   => max( 1, get_query_var( 'paged' ) ),
		'total'     => $wp_query->max_num_pages,
		'before_page_number' => '<span class="screen-reader-text">' . $translated . ' </span>',
	));
}
add_action( 'init', 'gm_wp_pagination' );

/** Custom View Article link to Post */
function gm_view_article( $more ) {
	global $post;
	return '... <a class="more-link" href="' . get_permalink( $post->ID ) . '">' . __( 'Read More' ) . '</a>';
}
add_filter( 'excerpt_more', 'gm_view_article' ); // Add 'View Article' button instead of [...] for Excerpts.

/** Add Editor Styles */
function gm_add_editor_styles() {
	add_editor_style( 'editor-style.css' );
}
add_action( 'after_setup_theme', 'gm_add_editor_styles' ); //Add Editor CSS.

/** Add fonts to editor */
function gm_add_font_editor_styles() {
	$font_url = str_replace( ',', '%2C', 'https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,700,700i' );
	add_editor_style( $font_url );
}
add_action( 'after_setup_theme', 'gm_add_font_editor_styles' ); // Add Fonts to Editor CSS.

/** Add font size and formats buttons */
function gm_mce_add_more_buttons( $buttons ) {

	array_unshift( $buttons, 'fontsizeselect' ); // Add Font Size Select.
	array_unshift( $buttons, 'styleselect' ); // Add Formats Select.
	return $buttons;
}
add_filter ( "mce_buttons_3", "gm_mce_add_more_buttons" );

/** Add font sizes to TinyMCE */
function gm_mce_inits( $init_array ) {
	$init_array['fontsize_formats'] = '10px 12px 13px 14px 15px 16px 18px 20px 22px 24px 26px 28px 30px 32px 34px 36px 38px 40px 48px 60px 72px';
	return $init_array;
}
add_filter( 'tiny_mce_before_init', 'gm_mce_inits' );

/** Callback function to filter the MCE settings */
function gm_mce_before_init_insert_formats( $init_array ) {

	// Define the style_formats array.
	$style_formats = array(
		// Each array child is a format with it's own settings.
		array(
			'title' => 'Teal Button',
			'inline' => 'a',
			'classes' => 'btn btn-teal',
			'selector' => 'a',
			'wrapper' => true,

		),
		array(
			'title' => 'Blue Button',
			'inline' => 'a',
			'classes' => 'btn btn-blue',
			'selector' => 'a',
			'wrapper' => true,
		),
		array(
			'title' => 'Orange Button',
			'inline' => 'a',
			'classes' => 'btn btn-orange',
			'selector' => 'a',
			'wrapper' => true,
		),
		array(
			'title' => 'Purple Button',
			'inline' => 'a',
			'classes' => 'btn btn-purple',
			'selector' => 'a',
			'wrapper' => true,
		),
		array(
			'title' => 'Yellow Button',
			'inline' => 'a',
			'classes' => 'btn btn-yellow',
			'selector' => 'a',
			'wrapper' => true,
		),
		array(
			'title' => 'Teal',
			'inline' => 'span',
			'classes' => 'teal',
			'wrapper' => true,
		),
		array(
			'title' => 'Blue',
			'inline' => 'span',
			'classes' => 'blue',
			'wrapper' => true,
		),
		array(
			'title' => 'Purple',
			'inline' => 'span',
			'classes' => 'purple',
			'wrapper' => true,
		),
		array(
			'title' => 'Orange',
			'inline' => 'span',
			'classes' => 'orange',
			'wrapper' => true,
		),
		array(
			'title' => 'Yellow',
			'inline' => 'span',
			'classes' => 'yellow',
			'wrapper' => true,
		),
	);
	// Insert the array, JSON ENCODED, into 'style_formats'.
	$init_array['style_formats'] = wp_json_encode( $style_formats );

	return $init_array;
}
// Attach callback to 'tiny_mce_before_init'.
add_filter( 'tiny_mce_before_init', 'gm_mce_before_init_insert_formats' );


/*------------------------------------*\
	ACF
\*------------------------------------*/

/**Hide ACF field group menu item */
//add_filter('acf/settings/show_admin', '__return_false');

/** Save theme-specific ACF field groups */
add_filter('acf/settings/save_json', function() {

	$paths = get_template_directory() . '/acf-json/';

	if ( is_child_theme() ) {
		$paths = get_stylesheet_directory() . '/acf-json/';
	}

	return $paths;

});

add_filter( 'acf/settings/load_json', function( $paths ) {

	unset( $paths[0] );

	$paths = array();

	if ( is_child_theme () )
	{
		$paths[] = get_stylesheet_directory() . '/acf-json';
	}
	$paths[] = get_template_directory() . '/acf-json';

	return $paths;
});

/*------------------------------------*\
	ACF POWERED THEME OPTIONS
\*------------------------------------*/
if ( function_exists( 'acf_add_options_page' ) ) {
	acf_add_options_page();
}

/*------------------------------------*\
	CUSTOM SLIDESHOW CODE
\*------------------------------------*/
/** Function to call slideshow */
function gm_khalsa_slider() {
	if ( get_field( 'slideshow_toggle' ) == 'dynamic' ) {
		$dynamic = ' slide-dynamic';
	}

	echo '<div class="loading flexslider';
	if ( ! empty( $dynamic ) ) {
		echo esc_attr( $dynamic );
	}
	echo '">';

	$slides = get_field( 'slideshow' );

	if ( $slides ) {
		echo '<ul class="slides">';
		$i = 0;
		foreach ( $slides as $slide ) {

			$image = $slide['image'];
			$size = 'hero';

			if ( get_field( 'slideshow_toggle' ) == 'dynamic' ) {

				echo '<li>';

				/*if ( 0 === $i ) {  //used in old gm slider code
					echo esc_attr( ' js-slide-active' );
				};
				echo '">';*/

				echo '<img class="slide-img" src="' . esc_url( wp_get_attachment_image_url( $image, $size ) ) . '">';

				if ( $slide['text_dynamic'] ) {
					echo '<div class="flex-caption is-active">';
				} else {
					echo '<div class="flex-caption">';
				}

				if ( $slide['text_dynamic'] ) {
					echo wp_kses_post( $slide['text_dynamic'] );
				}

				if ( $slide['link_dynamic'] ) {

					echo '<a class="slide-link" href="' . esc_url( $slide['link_dynamic']['url'] ) . '">';
					echo esc_html( $slide['link_dynamic']['title'] );
					echo '</a>';
				}


				echo '</div></li>';

			} elseif ( get_field( 'slideshow_toggle' ) == 'static' ) {

				echo '<li>';

				/*if ( 0 === $i ) {
					echo esc_attr( ' js-slide-active' );
				};*/

				echo '<img class="slide-img" src="' . esc_url( wp_get_attachment_image_url( $image, $size ) ) . '">';

				echo '</li>';
			}
			$i++;
		}

		echo '</ul>';
	}

	echo '</div>';

	if ( get_field( 'slideshow_toggle' ) == 'static' ) {
		if ( get_field( 'text_static' ) ) {
			echo '<div class="content-container">';
			echo '<div class="flex-caption is-active">';
		} else {
			echo '<div class="content-container">';
			echo '<div class="flex-caption">';
		}

		if ( get_field( 'text_static' ) ) {
			echo wp_kses_post( get_field( 'text_static' ) );
		}
		if ( get_field( 'link_static' ) ) {
			$link = get_field( 'link_static' );
			echo '<a class="slide-link" href="' . esc_url( $link['url'] ) . '">';
			echo esc_html( $link['title'] );
			echo '</a>';
		}

		echo '</div>';
	}
}

