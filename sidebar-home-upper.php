<?php
/**
 * Home Sidebar (Upper)
 *
 * This template is upper sidebar on the homepage template.
 *
 * @package WordPress
 * @subpackage Khalsa
 * @since GM 1.0
 */

?>
<!-- sidebar -->
<aside class="sidebar home-upper" role="complementary">
	
	<?php if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'sidebar-home-upper' ) ) ?>

</aside>
<!-- /sidebar -->
