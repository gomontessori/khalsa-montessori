<?php
/**
 * Default sidebar
 *
 * This template is the default sidebar for static pages
 *
 * @package WordPress
 * @subpackage Khalsa
 * @since GM 1.0
 */

?>
<!-- sidebar -->
<aside class="sidebar" role="complementary">
	
	<?php get_template_part( 'template-parts/sidebar-menu' ); ?>
	
	<?php if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'sidebar-widgets' ) ) ?>

</aside>
<!-- /sidebar -->
