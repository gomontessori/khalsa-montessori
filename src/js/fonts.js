(function( root, $, undefined ) {
	"use strict";

	$(function () {

      WebFont.load({
        google: {
            families: [ 'Poppins:400,400i,500,500i,700,700i' ]
        }
      });

	});

} ( this, jQuery ));
